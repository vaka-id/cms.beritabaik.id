import request from '@/utils/request'

export function getTopslideArticles(params) {
  return request({
    url: '/topslide-articles',
    method: 'get',
    params
  })
}
